$(document).ready(function () {

    $('.hamburger').click(function () {
        $('.fa-times').toggleClass("hiddenButton");
        $('.fa-bars').toggleClass("hiddenButton");
         $('.pageNavigation').slideToggle(500).toggleClass("hiddenButton");
    });

});
